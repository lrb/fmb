module git.envs.net/lrb/fmb

go 1.14

require (
	github.com/andybalholm/cascadia v1.2.0 // indirect
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/magiconair/properties v1.8.2 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/mitchellh/mapstructure v1.3.3 // indirect
	github.com/mmcdole/gofeed v1.0.0
	github.com/pelletier/go-toml v1.8.0 // indirect
	github.com/rs/xid v1.2.1 // indirect
	github.com/spf13/afero v1.3.4 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/cobra v1.0.0
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.7.1
	golang.org/x/net v0.0.0-20200822124328-c89045814202 // indirect
	golang.org/x/sys v0.0.0-20200828194041-157a740278f4 // indirect
	gopkg.in/ini.v1 v1.60.2 // indirect
)
