package db

import (
	"git.envs.net/lrb/fmb/models"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite" // Adds sqlite support to gorm
	"github.com/spf13/viper"
)

// Setup migrates the database schema if required
func Setup() {
	db := Connect()
	db.AutoMigrate(&models.Feed{})
	db.AutoMigrate(&models.Article{})
	db.Close()
}

// Connect finds the local db and returns it
func Connect() *gorm.DB {

	db, err := gorm.Open("sqlite3", viper.GetString("dbloc"))
	if err != nil {
		panic("failed to connect database")
	}
	return db
}
