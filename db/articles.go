package db

import (
	"fmt"
	"log"
	"os/exec"
	"runtime"

	"git.envs.net/lrb/fmb/models"
)

func AddArticle(item *models.Article) {
	db := Connect()
	defer db.Close()
	db.Create(item)
}

func GetUnreadArticles() []models.Article {
	db := Connect()
	defer db.Close()
	var articles []models.Article
	db.Where("Read = ?", false).Find(&articles)
	return articles

}

func GetArticleByID(id uint) {
	db := Connect()
	defer db.Close()
	var article models.Article
	db.Where("ID = ?", id).Find(&article)
	if (models.Article{} != article) {
		openbrowser(article.URL)
	} else {
		fmt.Println("Article not found.")
	}
}

func MarkArticleRead(id uint) {
	db := Connect()
	defer db.Close()
	db.Model(&models.Article{}).Where("ID = ?", id).Update("Read", true)

}

func MarkAllArticlesRead() {
	db := Connect()
	defer db.Close()
	var articles []models.Article
	db.Model(&articles).Select("Read").Updates(map[string]interface{}{"Read": true})
}

func MarkFeedRead(feedID uint) {
	db := Connect()
	defer db.Close()
	db.Model(&models.Article{}).Where("feed_id = ?", feedID).Update("Read", true)
}

func openbrowser(url string) {
	var err error

	switch runtime.GOOS {
	case "linux":
		err = exec.Command("xdg-open", url).Start()
	case "windows":
		err = exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
	case "darwin":
		err = exec.Command("open", url).Start()
	default:
		err = fmt.Errorf("unsupported platform")
	}
	if err != nil {
		log.Fatal(err)
	}

}
func CheckArticleExistsByURL(url string) bool {
	db := Connect()
	defer db.Close()
	var articles []models.Article
	db.Where("URL = ?", url).Find(&articles)
	if len(articles) > 0 {
		return true
	}
	return false
}
