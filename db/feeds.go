package db

import "git.envs.net/lrb/fmb/models"

func AddFeed(name, url string) {
	db := Connect()
	db.Create(&models.Feed{Name: name, Address: url})
	db.Close()
}

func RemoveFeed(id int) {
	db := Connect()
	db.Delete(&models.Feed{}, id)
	db.Close()
}

func ListFeeds() []models.Feed {
	db := Connect()
	var feeds []models.Feed
	db.Find(&feeds)
	db.Close()
	return feeds
}

func GetFeedNameByID(id uint) string {
	db := Connect()
	defer db.Close()
	var feed models.Feed
	db.Where("ID = ?", id).Find(&feed)
	return feed.Name
}

func FavFeed(id uint) {
	db := Connect()
	defer db.Close()
	db.Model(&models.Feed{}).Where("ID = ?", id).Update("Favourite", true)
}
