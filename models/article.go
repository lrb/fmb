package models

import (
	"github.com/jinzhu/gorm"
)

type Article struct {
	gorm.Model
	Fingerprint string
	Title       string
	Time        string
	Body        string
	URL         string
	Feed        Feed
	Feed_ID     uint
	Read        bool
}
