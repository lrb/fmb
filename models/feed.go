package models

import "github.com/jinzhu/gorm"

type Feed struct {
	gorm.Model
	Name      string
	Address   string
	Favourite bool
}
