/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"strconv"

	"git.envs.net/lrb/fmb/db"
	"github.com/spf13/cobra"
)

// readCmd represents the read command
var readCmd = &cobra.Command{
	Use:   "read",
	Short: "Read all articles with 'fmb read', or open a specific article with 'fmd read [article-id]'",
	Long:  `Read all articles with 'fmb read', or open a specific article with 'fmd read [article-id]'`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			articles := db.GetUnreadArticles()
			for _, v := range articles {
				fmt.Println("[" + strconv.FormatUint(uint64(v.ID), 10) + "][" + db.GetFeedNameByID(v.Feed_ID) + "] " + v.Title)
			}
		} else {
			artID, _ := strconv.ParseUint(args[0], 10, 64)
			db.GetArticleByID(uint(artID))
			db.MarkArticleRead(uint(artID))

		}

	},
}

func init() {
	rootCmd.AddCommand(readCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// readCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// readCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
