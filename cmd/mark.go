/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"strconv"

	"git.envs.net/lrb/fmb/db"
	"github.com/spf13/cobra"
)

// markCmd represents the mark command
var markCmd = &cobra.Command{
	Use:   "mark",
	Short: "Mark an article read without opening it (fmb mark [article-id])",
	Long:  `Mark an article read without opening it (fmb mark [article-id])`,
	Run: func(cmd *cobra.Command, args []string) {
		if args[0] == "all" {
			db.MarkAllArticlesRead()
			fmt.Println("All articles marked as read.")
		} else if args[0] == "feed" {
			feedID, _ := strconv.ParseUint(args[1], 10, 64)
			db.MarkFeedRead(uint(feedID))
			feedName := db.GetFeedNameByID(uint(feedID))
			fmt.Println(feedName + " has been marked as read.")
		} else {
			artID, _ := strconv.ParseUint(args[0], 10, 64)
			db.MarkArticleRead(uint(artID))
			fmt.Println("Article " + args[0] + " marked as read.")

		}
	},
}

func init() {
	rootCmd.AddCommand(markCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// markCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// markCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
