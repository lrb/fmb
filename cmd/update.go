/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"log"

	"git.envs.net/lrb/fmb/models"

	"git.envs.net/lrb/fmb/db"
	"github.com/mmcdole/gofeed"
	"github.com/spf13/cobra"
)

// updateCmd represents the update command
var updateCmd = &cobra.Command{
	Use:   "update",
	Short: "Updates all feeds",
	Long:  `Updates all feeds.`,
	Run: func(cmd *cobra.Command, args []string) {
		feeds := db.ListFeeds()
		p := gofeed.NewParser()
		var arts []models.Article
		for _, v := range feeds {
			f, err := p.ParseURL(v.Address)
			if err != nil {
				fmt.Println(err.Error())
				log.Fatal()
			}
			for _, item := range f.Items {

				var article models.Article
				article.Title = item.Title
				article.Body = item.Content
				article.URL = item.Link
				article.Time = item.Published
				article.Feed = v
				article.Fingerprint = item.Title + item.Link
				article.Read = false
				arts = append(arts, article)

			}
			fmt.Println(v.Name + " updated.")
		}
		for _, art := range arts {
			if db.CheckArticleExistsByURL(art.URL) == false {

				db.AddArticle(&art)
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(updateCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// updateCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// updateCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
