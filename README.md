# fmb

A minimal CLI feed reader written in Go. Very much a work in progress.

fmb --help for commands

## Features
* Add feeds
`fmb add "https://feed.url"`
* List feeds
`fmb list`
* Remove feeds
`fmb remove 12` (where 12 is the feed's ID)
* Update feeds
`fmb update`
* View unread articles
`fmb read`
* Read articles
`fmb read 12` (where 12 is the article's ID)
* Manually mark articles as read
`fmb mark 12` (where 12 is the article's ID)
* Mark all articles from every feed read 
`fmb mark all`
* Mark all articles from ONE feed read
`fmb mark feed 12` (where 12 is the feed's ID)

## Coming soon
* Fixing where db/config are created (currently created in the directory you run fmb in)
* Option to update simple HTML file when feeds are updated to browse easily